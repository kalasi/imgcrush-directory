# Change Log
All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning][semver].

## [Unreleased]

### Changed

### Added

## [1.0.0] - 2016-02-12

Initial commit.

[Unreleased]: https://gitlab.com/kalasi/dotenv.rs/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/kalasi/imgcrush-directory/compare/307d589...v1.0.0
[semver]: http://semver.org
