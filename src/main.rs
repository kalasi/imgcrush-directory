// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
// software, either in source code form or as a compiled binary, for any purpose,
// commercial or non-commercial, and by any means.
//
// In jurisdictions that recognize copyright laws, the author or authors of this
// software dedicate any and all copyright interest in the software to the public
// domain. We make this dedication for the benefit of the public at large and to
// the detriment of our heirs and successors. We intend this dedication to be an
// overt act of relinquishment in perpetuity of all present and future rights to
// this software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to http://unlicense.org

extern crate glob;

mod filetypes;

use std::env;
use std::fs;
use std::path::{PathBuf, Path};
use std::process::Command;

#[derive(Debug)]
pub enum ImgCrushError {
    NoParentPath,
    PathRetrieval,
    ProgramError,
    RenameError,
    UnknownFiletype,
}

#[allow(non_camel_case_types)]
#[derive(Clone, Debug)]
pub enum Filetype {
    gif,
    jpg,
    png,
}

struct CrushingResults {
    pub files_traversed: i32,
    pub total_saved: i32,
}

impl CrushingResults {
    pub fn new() -> CrushingResults {
        CrushingResults {
            files_traversed: 0,
            total_saved: 0,
        }
    }
}

#[allow(dead_code)]
pub struct FileSavings {
    bytes_saved: i64,
    percent_decrease: f64,
    size_after: i64,
    size_prior: i64,
}

impl FileSavings {
    pub fn new(prior: i64, after: i64) -> FileSavings {
        FileSavings {
            bytes_saved: prior - after,
            percent_decrease: (after as f64) / (prior as f64),
            size_after: after,
            size_prior: prior,
        }
    }
}

pub fn filesize(filepath: &str) -> i64 {
    fs::metadata(Path::new(filepath)).unwrap().len() as i64
}

#[allow(dead_code)]
fn has_command(command: &str) -> bool {
    match Command::new("which").arg(format!("type {}", command)).status() {
        Ok(status) => status.success(),
        Err(e) => panic!("Error checking if has command {}: {}", command, e),
    }
}

fn handle_file(filebuf: PathBuf, filetype: Filetype) -> Result<FileSavings, ImgCrushError> {
    let filepath: &str = match filebuf.to_str() {
        Some(filepath) => filepath,
        None => {
            println!("Error retrieving filepath for: {:?}", filebuf);

            return Err(ImgCrushError::PathRetrieval);
        },
    };

    println!("Crushing {}", filebuf.to_str().unwrap());

    match filetype {
        Filetype::gif => filetypes::gif::run(filepath),
        Filetype::jpg => filetypes::jpg::run(filepath),
        Filetype::png => filetypes::png::run(filepath),
    }
}


fn shell_exec(directory: &str, filetype: Filetype) -> CrushingResults {
    let results: CrushingResults = CrushingResults::new();

    let filetype_str = match filetype.clone() {
        Filetype::gif => "gif",
        Filetype::jpg => "jp*g",
        Filetype::png => "png",
    };

    let pattern = format!("{}/**/*.{}",
                          directory,
                          filetype_str);

    println!("{}", pattern);

    for file in glob::glob(&pattern).unwrap() {
        match file {
            Ok(path) => {
                let _ = handle_file(path, filetype.clone());
            },
            Err(why) => println!("Couldn't handle file: {}", why),
        }
    }

    results
}

fn main() {
    if env::args().count() < 2 {
        println!("A directory path must be supplied.");

        return;
    }

    let directory: &str = &env::args().nth(1).unwrap()[..];

    let mut enable_gif = false;
    let mut enable_jpg = false;
    let mut enable_png = false;
    let mut flags = false;

    let arg_flags: Vec<String> = env::args()
        .skip(2)
        .collect();

    for flag in arg_flags {
        flags = true;

        let flag_str: &str = &flag[..];

        match flag_str {
            "--gif" => {
                enable_gif = true;
            },
            "--jpg" => {
                enable_jpg = true;
            },
            "--png" => {
                enable_png = true;
            },
            _ => panic!("Unknown flag: {}", flag),
        }
    }

    let has_gifsicle: bool = true;
    let has_jpegoptim: bool = true;
    let has_pngcrush: bool = true;

    let gif_do: bool = if flags {
        enable_gif
    } else {
        has_gifsicle
    };
    let jpg_do: bool = if flags {
        enable_jpg
    } else {
        has_jpegoptim
    };
    let png_do: bool = if flags {
        enable_png
    } else {
        has_pngcrush
    };

    let mut results: CrushingResults = CrushingResults::new();

    if gif_do {
        let gif_results: CrushingResults = shell_exec(directory, Filetype::gif);

        results.files_traversed += gif_results.files_traversed;
        results.total_saved += gif_results.total_saved;

        println!(".gif saved: {}b", gif_results.total_saved);
        println!(".gif traversed: {}", gif_results.files_traversed);
    }

    if jpg_do {
        let jpg_results: CrushingResults = shell_exec(directory, Filetype::jpg);

        results.files_traversed += jpg_results.files_traversed;
        results.total_saved += jpg_results.total_saved;

        println!(".jpg saved: {}b", jpg_results.total_saved);
        println!(".jpg traversed: {}", jpg_results.files_traversed);
    }

    if png_do {
        let png_results: CrushingResults = shell_exec(directory, Filetype::png);

        results.files_traversed += png_results.files_traversed;
        results.total_saved += png_results.total_saved;

        println!(".png saved: {}b", png_results.total_saved);
        println!(".png traversed: {}", png_results.files_traversed);
    }

    println!("Files traversed: {}", results.files_traversed);
    println!("Total saved: {}b", results.total_saved);
}
