// This is free and unencumbered software released into the public domain.
//
// Anyone is free to copy, modify, publish, use, compile, sell, or distribute this
// software, either in source code form or as a compiled binary, for any purpose,
// commercial or non-commercial, and by any means.
//
// In jurisdictions that recognize copyright laws, the author or authors of this
// software dedicate any and all copyright interest in the software to the public
// domain. We make this dedication for the benefit of the public at large and to
// the detriment of our heirs and successors. We intend this dedication to be an
// overt act of relinquishment in perpetuity of all present and future rights to
// this software under copyright law.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
// ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
// WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//
// For more information, please refer to http://unlicense.org

use super::super::{FileSavings, ImgCrushError, filesize};
use std::fs;
use std::process::{Command, Stdio};

pub fn run(filepath: &str) -> Result<FileSavings, ImgCrushError> {
    pngcrush(filepath)
}

fn pngcrush(filepath: &str) -> Result<FileSavings, ImgCrushError> {
    let path_temp: &str = &{
        let mut temp: String = String::from(filepath);

        temp.push('_');

        temp
    }[..];

    let filesize_prior: i64 = filesize(filepath);

    let args: Vec<&str> = vec![filepath, path_temp];

    let status = Command::new("pngcrush")
        .args(&args)
        .stdout(Stdio::null())
        .status();

    match status {
        Ok(status) => if !status.success() {
            return Err(ImgCrushError::ProgramError);
        },
        Err(why) => {
            println!("Error running pngcrush on {}: {}", filepath, why);

            return Err(ImgCrushError::ProgramError);
        },
    };

    match fs::rename(path_temp, filepath) {
        Ok(_) => {},
        Err(why) => {
            println!("Error renaming {} to {}: {}", path_temp, filepath, why);

            return Err(ImgCrushError::RenameError);
        },
    }

    let filesize_after: i64 = filesize(filepath);

    Ok(FileSavings::new(filesize_prior, filesize_after))
}
