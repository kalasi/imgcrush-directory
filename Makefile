build:
	cargo build

install:
	@cargo build --release
	@cp target/release/imgcrush-directory /usr/local/bin/imgcrush-directory
	@echo "imgcrush-directory installed as 'imgcrush-directory'."
