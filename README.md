[![ci-badge][]][ci] [![license-badge][]][license]

# imgcrush-directory

[ci]: https://gitlab.com/kalasi/imgcrush-directory/pipelines
[ci-badge]: https://gitlab.com/kalasi/imgcrush-directory/badges/master/build.svg
[license]: http://unlicense.org/
[license-badge]: https://img.shields.io/badge/license-UNLICENSE-lightgrey.svg
